part of 'food_bloc.dart';

abstract class FoodState extends Equatable {
  const FoodState();

  @override
  List get props => [];
}

class FoodInitial extends FoodState {}

class FoodLoading extends FoodState {}

class FoodLoaded extends FoodState {
  final List<FoodModel> foodList;

  const FoodLoaded(this.foodList);

  @override
  List get props => [foodList];
}

class FoodError extends FoodState {
  final String message;

  const FoodError(this.message);

  @override
  List get props => [message];
}
