part of 'food_bloc.dart';

abstract class FoodEvent extends Equatable {
  const FoodEvent();
}

class GetListFood extends FoodEvent {
  @override
  // TODO: implement props
  List<Object?> get props => throw UnimplementedError();
}

class AddFood extends FoodEvent {
  final String name;
  final String price;
  final String image;
  final String description;
  final BuildContext context;

  const AddFood({
    required this.name,
    required this.price,
    required this.image,
    required this.description,
    required this.context
  });

  @override
  List<Object> get props => [name, price, image, description, context];
}
