import 'package:dio/dio.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:order_food_bloc/models/food_model.dart';
import 'package:order_food_bloc/services/api_food_service.dart';

import '../../services/api_repository.dart';

part 'food_state.dart';

part 'food_event.dart';

class FoodBloc extends Bloc<FoodEvent, FoodState> {
  final ApiRepository apiRepository = ApiRepository();
  final ApiFoodService apiFoodService = ApiFoodService();

  FoodBloc() : super(FoodInitial()) {
    on<GetListFood>((event, emit) async {
      try {
        emit(FoodLoading());
        final getFood = await apiRepository.getListFood();
        emit(FoodLoaded(getFood));
      } catch (e) {
        emit(FoodError(e.toString()));
      }
    });

    on<AddFood>((event, emit) async {
      emit(FoodLoading());
      Response res = await apiFoodService.addFood(
          event.name, event.image, event.price, event.description);
      if (res.statusCode == 201) {
        final list = await apiRepository.getListFood();
        emit(FoodLoaded(list));
      } else {
        emit(const FoodError("Loi"));
      }
    });
  }
}
