part of 'order_bloc.dart';

abstract class OrderState extends Equatable {
  const OrderState();

  @override
  List<Object> get props => [];
}

class OrderInitial extends OrderState {}

class OrderLoading extends OrderState {}

class OrderLoaded extends OrderState {
  final List<OrderModel> orderFoodList;

  const OrderLoaded(this.orderFoodList);

  @override
  List<Object> get props => [orderFoodList];
}

class OrderSuccess extends OrderState {
  final List<OrderModel> orderFoodList;

  const OrderSuccess(this.orderFoodList);

  @override
  List<Object> get props => [orderFoodList];
}

class OrderError extends OrderState {
  final String message;

  const OrderError(this.message);

  @override
  List<Object> get props => [message];
}
