import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

import '../../models/order_model.dart';

part 'order_event.dart';

part 'order_state.dart';

class OrderBloc extends Bloc<OrderEvent, OrderState> {
  final List<OrderModel> orderListFood = [];

  OrderBloc() : super(OrderInitial()) {
    on<AddFoodOrder>((event, emit) {
      emit(OrderLoading());
      int index = -1;
      try {
        bool check = false;
        for (var element in orderListFood) {
          index++;
          if (element.id == event.id) {
            int quan =
                int.parse(element.quantity ?? "0") + int.parse(event.quantity);
            orderListFood[index] = element.copyWith(quantity: quan.toString());
            check = true;
          }
        }

        if (check) {
        } else {
          OrderModel order = OrderModel(
              id: event.id,
              name: event.name,
              price: event.price,
              image: event.image,
              description: event.description,
              quantity: event.quantity);
          orderListFood.add(order);
        }
        emit(OrderSuccess(orderListFood));
      } catch (e) {
        emit(OrderError(e.toString()));
      }
    });
  }
}
