part of 'order_bloc.dart';

abstract class OrderEvent extends Equatable {
  const OrderEvent();

  @override
  List<Object> get props => [];
}

class AddFoodOrder extends OrderEvent {
  final String name, id;
  final double price;
  final String image;
  final String description;
  final String quantity;

  const AddFoodOrder({
    required this.id,
    required this.name,
    required this.price,
    required this.image,
    required this.description,
    required this.quantity,
  });

  @override
  List<Object> get props => [id, name, price, image, description, quantity];
}
