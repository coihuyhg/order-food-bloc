import '../models/food_model.dart';
import 'api_food_service.dart';

class ApiRepository {
  final ApiFoodService apiFoodService = ApiFoodService();

  Future<List<FoodModel>> getListFood() async {
    var getFood = await apiFoodService.getFood();
    return getFood;
  }
}
