import 'package:dio/dio.dart';
import 'package:order_food_bloc/models/food_model.dart';

class ApiFoodService {
  Future<List<FoodModel>> getFood() async {
    var response =
        await Dio().get("https://63eafe34f1a969340db02811.mockapi.io/food");
    List<FoodModel> newFood = (response.data as List)
        .map((item) => FoodModel.fromJson(item))
        .toList();
    return newFood;
  }

  Future<Response> addFood(
      String name, String image, String price, String description) async {
    var addFood = await Dio().post(
        "https://63eafe34f1a969340db02811.mockapi.io/food",
        data: {
          "name": name,
          "price": price,
          "image": image,
          "description": description
        },
        options: Options(headers: {"Content-Type": "application/json"}));
    return addFood;
  }
}
