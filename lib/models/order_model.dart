class OrderModel {
  String? id;
  String? name;
  double? price;
  String? image;
  String? description;
  String? quantity;

  OrderModel({
    this.id,
    this.name,
    this.price,
    this.image,
    this.description,
    this.quantity,
  });

  OrderModel copyWith({
    String? id,
    String? name,
    double? price,
    String? image,
    String? description,
    String? quantity,
  }) =>
      OrderModel(
        id: id ?? this.id,
        name: name ?? this.name,
        price: price ?? this.price,
        image: image ?? this.image,
        description: description ?? this.description,
        quantity: quantity ?? this.quantity,
      );
}
