// To parse this JSON data, do
//
//     final foodModel = foodModelFromJson(jsonString);

import 'dart:convert';

List<FoodModel> foodModelFromJson(String str) => List<FoodModel>.from(json.decode(str).map((x) => FoodModel.fromJson(x)));

String foodModelToJson(List<FoodModel> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class FoodModel {
  DateTime? createdAt;
  String? name;
  String? image;
  String? price;
  String? description;
  String? id;

  FoodModel({
    this.createdAt,
    this.name,
    this.image,
    this.price,
    this.description,
    this.id,
  });

  FoodModel copyWith({
    DateTime? createdAt,
    String? name,
    String? image,
    String? price,
    String? description,
    String? id,
  }) =>
      FoodModel(
        createdAt: createdAt ?? this.createdAt,
        name: name ?? this.name,
        image: image ?? this.image,
        price: price ?? this.price,
        description: description ?? this.description,
        id: id ?? this.id,
      );

  factory FoodModel.fromJson(Map<String, dynamic> json) => FoodModel(
    createdAt: DateTime.parse(json["createdAt"]),
    name: json["name"],
    image: json["image"],
    price: json["price"],
    description: json["description"],
    id: json["id"],
  );

  Map<String, dynamic> toJson() => {
    "createdAt": createdAt?.toIso8601String(),
    "name": name,
    "image": image,
    "price": price,
    "description": description,
    "id": id,
  };
}
