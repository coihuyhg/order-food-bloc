import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:order_food_bloc/bloc/add_food_bloc/food_bloc.dart';
import 'package:order_food_bloc/bloc/order_food_bloc/order_bloc.dart';

import 'screens/main_food_screen.dart';

void main() {
  runApp(
    MultiBlocProvider(
      providers: [
        BlocProvider<FoodBloc>(create: (context) => FoodBloc()),
        BlocProvider<OrderBloc>(create: (context) => OrderBloc()),
      ],
      child: const MyApp(),
    ),
  );
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      title: "Order Food",
      home: MainTab(),
    );
  }
}
