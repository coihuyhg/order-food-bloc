import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:order_food_bloc/bloc/add_food_bloc/food_bloc.dart';

import 'home_screen/home_food_screen.dart';
import 'order_screen/order_food_screen.dart';

class MainTab extends StatefulWidget {
  const MainTab({Key? key}) : super(key: key);

  @override
  State<MainTab> createState() => _MainTabState();
}

class _MainTabState extends State<MainTab> {
  int selected = 0;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    BlocProvider.of<FoodBloc>(context).add(GetListFood());
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        bottomNavigationBar: menu(),
        body: const TabBarView(
          children: [HomeOrderFood(), OrderFood()],
        ),
      ),
    );
  }

  Widget menu() {
    return TabBar(
      onTap: (index) {
        setState(() {
          selected = index;
        });
      },
      labelStyle: const TextStyle(fontSize: 12),
      unselectedLabelStyle: const TextStyle(fontSize: 12),
      indicatorColor: Colors.transparent,
      labelColor: const Color(0xFF00A3FF),
      unselectedLabelColor: const Color(0xFF6B6B6B),
      tabs: [
        Tab(
            text: "Home",
            icon: selected == 0
                ? Image.asset("assets/icons/ic_home_selected.png")
                : Image.asset("assets/icons/ic_home_unselected.png")),
        Tab(
            text: "Order",
            icon: selected == 1
                ? Image.asset("assets/icons/ic_order_selected.png")
                : Image.asset("assets/icons/ic_order_unselected.png")),
      ],
    );
  }
}
