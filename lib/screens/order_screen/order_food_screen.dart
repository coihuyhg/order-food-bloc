import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:order_food_bloc/bloc/order_food_bloc/order_bloc.dart';
import 'package:order_food_bloc/screens/detail_screen/detail_food_screen.dart';

import '../../widgets/app_bar_widget.dart';

class OrderFood extends StatefulWidget {
  const OrderFood({Key? key}) : super(key: key);

  @override
  State<OrderFood> createState() => _OrderFoodState();
}

class _OrderFoodState extends State<OrderFood> {
  int index = -1;
  double total = 0;
  int quantity = 0;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBarWidget(title: "order"),
        body: Column(
          children: [
            BlocBuilder<OrderBloc, OrderState>(
              builder: (context, state) {
                if (state is OrderLoading) {
                  return const Center(child: CircularProgressIndicator());
                } else if (state is OrderSuccess) {
                  return Expanded(
                      child: ListView.builder(
                    itemCount: state.orderFoodList.length,
                    itemBuilder: (context, index) {
                      return Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(
                                left: 20, right: 20, top: 18, bottom: 21),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                SizedBox(
                                  height: 100,
                                  width: 100,
                                  child: Image.network(
                                    "${state.orderFoodList[index].image}",
                                    errorBuilder: (context, error,
                                            stackTrace) =>
                                        Image.network(
                                            "https://thuemaychuao.net/wp-content/uploads/2021/10/img.gif"),
                                  ),
                                ),
                                const SizedBox(width: 33),
                                Expanded(
                                  flex: 2,
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        "${state.orderFoodList[index].name}",
                                        style: const TextStyle(
                                            fontSize: 16,
                                            fontWeight: FontWeight.bold),
                                      ),
                                      Text(
                                        "\$${state.orderFoodList[index].price}",
                                        style: const TextStyle(
                                            fontSize: 16,
                                            color: Color(0xFFFF1F00)),
                                      ),
                                      Text(
                                        "${state.orderFoodList[index].quantity}",
                                        style: const TextStyle(fontSize: 16),
                                      ),
                                    ],
                                  ),
                                ),
                                IconButton(
                                    onPressed: () {
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (ctx) =>
                                                  DetailFoodScreen(
                                                      item: state.orderFoodList,
                                                      index: index)));
                                    },
                                    icon: const Icon(
                                      Icons.keyboard_arrow_right,
                                      color: Color(0xFF6B6B6B),
                                    ))
                              ],
                            ),
                          ),
                          const Padding(
                              padding: EdgeInsets.only(left: 12, right: 12),
                              child: Divider())
                        ],
                      );
                    },
                  ));
                } else if (state is OrderError) {
                  return Center(child: Text(state.message));
                } else {
                  return Container();
                }
              },
            ),
            BlocBuilder<OrderBloc, OrderState>(
              builder: (context, state) {
                if (state is OrderSuccess) {
                  for (var element in state.orderFoodList) {
                    quantity += int.parse(element.quantity ?? "0");
                    total += double.parse(element.price.toString()) *
                        int.parse(element.quantity.toString());
                  }
                  return Padding(
                    padding:
                        const EdgeInsets.only(left: 18, right: 18, bottom: 34),
                    child: Column(
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [const Text("Quantity"), Text("$quantity")],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [const Text("Total"), Text("\$$total")],
                        ),
                      ],
                    ),
                  );
                }
                return Container(color: Colors.red);
              },
            )
          ],
        ));
  }
}
