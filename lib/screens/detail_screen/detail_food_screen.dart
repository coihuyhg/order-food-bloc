import 'package:flutter/material.dart';
import 'package:order_food_bloc/models/order_model.dart';

import '../../widgets/app_bar_widget.dart';

class DetailFoodScreen extends StatefulWidget {
  final List<OrderModel> item;
  final int index;

  const DetailFoodScreen({Key? key, required this.index, required this.item})
      : super(key: key);

  @override
  State<DetailFoodScreen> createState() => _DetailFoodScreenState();
}

class _DetailFoodScreenState extends State<DetailFoodScreen> {
  double totalPrice = 0;

  @override
  void initState() {
    // TODO: implement initState
    totalPrice += double.parse(widget.item[widget.index].price.toString()) *
        int.parse(widget.item[widget.index].quantity.toString());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBarWidget(
        title: "${widget.item[widget.index].name}",
        leading: IconButton(
          onPressed: () {
            Navigator.of(context).pop();
          },
          icon: const Icon(Icons.keyboard_arrow_left),
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.only(left: 18, right: 18, top: 18),
        child: Column(
          children: [
            SizedBox(
              height: 200,
              width: MediaQuery.of(context).size.width,
              child: Image.network(
                "${widget.item[widget.index].image}",
                errorBuilder: (context, error, stackTrace) => Image.network(
                    "https://thuemaychuao.net/wp-content/uploads/2021/10/img.gif"),
              ),
            ),
            const SizedBox(height: 21),
            Text("${widget.item[widget.index].description}"),
            const SizedBox(height: 23),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                const Text("Price",
                    style: TextStyle(fontWeight: FontWeight.bold)),
                Text("\$${widget.item[widget.index].price}"),
              ],
            ),
            const SizedBox(height: 16),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                const Text("Quantity",
                    style: TextStyle(fontWeight: FontWeight.bold)),
                Text("${widget.item[widget.index].quantity}"),
              ],
            ),
            const SizedBox(height: 16),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                const Text("Total",
                    style: TextStyle(fontWeight: FontWeight.bold)),
                Text("\$$totalPrice"),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
