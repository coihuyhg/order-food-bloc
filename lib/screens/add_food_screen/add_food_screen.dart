import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:order_food_bloc/bloc/add_food_bloc/food_bloc.dart';
import 'package:order_food_bloc/widgets/app_bar_widget.dart';

import '../../widgets/text_form_field_widget.dart';

class AddFoodScreen extends StatefulWidget {
  const AddFoodScreen({Key? key}) : super(key: key);

  @override
  State<AddFoodScreen> createState() => _AddFoodScreenState();
}

class _AddFoodScreenState extends State<AddFoodScreen> {
  late TextEditingController nameController;
  late TextEditingController priceController;
  late TextEditingController imageController;
  late TextEditingController descriptionController;

  @override
  void initState() {
    nameController = TextEditingController();
    priceController = TextEditingController();
    imageController = TextEditingController();
    descriptionController = TextEditingController();
    // TODO: implement initState
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  void addFood() {
    final name = nameController.text;
    final price = priceController.text;
    final image = imageController.text;
    final description = descriptionController.text;
    if (name.isEmpty) {
      ScaffoldMessenger.of(context)
          .showSnackBar(const SnackBar(content: Text('Vui lòng nhập name')));
    } else if (price.isEmpty) {
      ScaffoldMessenger.of(context)
          .showSnackBar(const SnackBar(content: Text('Vui lòng nhập price')));
    } else if (image.isEmpty) {
      ScaffoldMessenger.of(context)
          .showSnackBar(const SnackBar(content: Text('Vui lòng nhập image')));
    } else if (description.isEmpty) {
      ScaffoldMessenger.of(context)
          .showSnackBar(const SnackBar(content: Text('Vui lòng nhập description')));
    }
    if (name.isNotEmpty &&
        price.toString().isNotEmpty &&
        image.isNotEmpty &&
        description.isNotEmpty) {
      BlocProvider.of<FoodBloc>(context).add(
        AddFood(
          name: name,
          price: price,
          image: image,
          description: description,
          context: context,
        ),
      );
      ScaffoldMessenger.of(context)
          .showSnackBar(const SnackBar(content: Text('Đã thêm đồ ăn')));
      Navigator.pop(context);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBarWidget(
          title: "Add",
          leading: IconButton(
              onPressed: () {
                Navigator.of(context).pop();
              },
              icon: const Icon(Icons.keyboard_arrow_left))),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.only(left: 50, right: 50, top: 24),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Text("Name"),
              const SizedBox(height: 6),
              TextFormFiledWidget(
                controller: nameController,
                hintText: "Enter name",
              ),
              const SizedBox(height: 20),
              const Text("Price"),
              const SizedBox(height: 6),
              TextFormFiledWidget(
                controller: priceController,
                hintText: "Enter price",
              ),
              const SizedBox(height: 20),
              const Text("Image"),
              const SizedBox(height: 6),
              TextFormFiledWidget(
                controller: imageController,
                hintText: "Enter image link",
              ),
              const SizedBox(height: 20),
              const Text("Description"),
              const SizedBox(height: 6),
              TextFormFiledWidget(
                controller: descriptionController,
                hintText: "Description",
              ),
            ],
          ),
        ),
      ),
      bottomSheet: Container(
        width: MediaQuery.of(context).size.width,
        height: 90,
        padding: const EdgeInsets.only(left: 50, right: 50, bottom: 42),
        child: ElevatedButton(
          onPressed: addFood,
          child: const Text("Save"),
        ),
      ),
    );
  }
}
