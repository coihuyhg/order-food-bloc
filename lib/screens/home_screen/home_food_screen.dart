import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:order_food_bloc/bloc/add_food_bloc/food_bloc.dart';

import '../../bloc/order_food_bloc/order_bloc.dart';
import '../../models/food_model.dart';
import '../../widgets/app_bar_widget.dart';
import '../add_food_screen/add_food_screen.dart';

class HomeOrderFood extends StatefulWidget {
  const HomeOrderFood({Key? key}) : super(key: key);

  @override
  State<HomeOrderFood> createState() => _HomeOrderFoodState();
}

class _HomeOrderFoodState extends State<HomeOrderFood> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBarWidget(title: "Home"),
      body: BlocBuilder<FoodBloc, FoodState>(
        builder: (context, state) {
          if (state is FoodLoading) {
            return const Center(
              child: CircularProgressIndicator(),
            );
          } else if (state is FoodLoaded) {
            return ListView.builder(
              itemCount: state.foodList.length,
              shrinkWrap: true,
              itemBuilder: (context, index) {
                FoodModel foods = state.foodList[index];
                return Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(
                          left: 20, right: 20, top: 18, bottom: 21),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          SizedBox(
                            height: 100,
                            width: 100,
                            child: Image.network(
                              "${foods.image}",
                              errorBuilder: (context, error, stackTrace) =>
                                  Image.network(
                                      "https://thuemaychuao.net/wp-content/uploads/2021/10/img.gif"),
                            ),
                          ),
                          const SizedBox(width: 33),
                          Expanded(
                            flex: 2,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  "${foods.name}",
                                  style: const TextStyle(
                                      fontSize: 16,
                                      fontWeight: FontWeight.bold),
                                ),
                                const SizedBox(height: 14),
                                Text(
                                  "${foods.price}",
                                  style: const TextStyle(
                                      fontSize: 16, color: Color(0xFFFF1F00)),
                                ),
                              ],
                            ),
                          ),
                          IconButton(
                              onPressed: () {
                                BlocProvider.of<OrderBloc>(context)
                                    .add(AddFoodOrder(
                                  id: foods.id.toString(),
                                  name: foods.name.toString(),
                                  price: double.parse(foods.price.toString()),
                                  image: foods.image.toString(),
                                  description: foods.description.toString(),
                                  quantity: "1",
                                ));
                                ScaffoldMessenger.of(context).showSnackBar(
                                  const SnackBar(
                                      duration: Duration(milliseconds: 350),
                                      content: Text("Đã thêm vào giỏ hàng")),
                                );
                              },
                              icon: const Icon(Icons.shopping_cart,
                                  color: Color(0xFF00A3FF))),
                        ],
                      ),
                    ),
                    const Padding(
                        padding: EdgeInsets.only(left: 12, right: 12),
                        child: Divider(
                          thickness: 2,
                        ))
                  ],
                );
              },
            );
          } else if (state is FoodError) {
            return Center(child: Text(state.message));
          } else {
            return Container();
          }
        },
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(context,
              MaterialPageRoute(builder: (context) => const AddFoodScreen()));
        },
        tooltip: "Order",
        child: const Icon(Icons.add),
      ),
    );
  }
}
